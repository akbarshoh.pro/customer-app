package com.example.myapplication.domain

import com.example.myapplication.data.model.ProductData
import kotlinx.coroutines.flow.Flow

interface BasketRepository {
    fun getProducts(data:List<String>): Flow<ArrayList<ProductData>>
    fun setBasket(balance:Int):Flow<Result<Unit>>
}