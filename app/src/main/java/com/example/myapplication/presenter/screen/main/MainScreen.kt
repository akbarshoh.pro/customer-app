package com.example.myapplication.presenter.screen.main

import android.annotation.SuppressLint
import androidx.compose.material.BottomNavigation
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.tab.CurrentTab
import cafe.adriel.voyager.navigator.tab.TabNavigator
import com.example.myapplication.presenter.screen.pages.basket.BasketPage
import com.example.myapplication.presenter.screen.pages.home.HomePages
import com.example.myapplication.presenter.screen.pages.profile.ProfilePage
import com.example.myapplication.utils.TabNavigatorItem

class MainScreen:Screen {
    @Composable
    override fun Content() {
        MainContent()
    }
}


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
private fun MainContent(
//    uiState: MainContract.UIState,
//    onEventDispatcher: (MainContract.Intent) -> Unit
) {
    TabNavigator(tab = HomePages) {
        Scaffold(
            content = {
                CurrentTab()

            },
            bottomBar = {
                BottomNavigation {
                    TabNavigatorItem(tab = HomePages)
                    TabNavigatorItem(tab = BasketPage)
                    TabNavigatorItem(tab = ProfilePage)
                }
            }
        )
    }
}