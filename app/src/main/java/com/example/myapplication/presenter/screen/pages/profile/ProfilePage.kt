package com.example.myapplication.presenter.screen.pages.profile

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.magnifier
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RadialGradientShader
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import com.example.myapplication.R
import com.example.myapplication.ui.theme.MyApplicationTheme

object ProfilePage : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val title = "Profile"
            val icon =
                rememberVectorPainter(image = ImageVector.vectorResource(id = R.drawable.ic_profile))

            return remember {
                TabOptions(
                    index = 0u,
                    title = title,
                    icon = icon
                )
            }
        }

    @Composable
    override fun Content() {
        MyApplicationTheme {
            ProfileContent()
        }
    }
}


@SuppressLint("ResourceAsColor")
@Composable
fun ProfileContent() {
    Surface(
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.223f)

            ) {
                Image(
                    modifier = Modifier.fillMaxSize(),
                    painter = painterResource(id = R.drawable.bg_profile),
                    contentDescription = null,
                )
                Column(Modifier.fillMaxSize()) {
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp),
                        text = "Shahsiy kabinet", fontSize = 16.sp,
                        color = Color.White,
                        textAlign = TextAlign.Center
                    )
                    Row(modifier = Modifier.fillMaxSize()) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_user), 
                            contentDescription = null,
                            modifier = Modifier
                                .padding(start = 16.dp)
                                .align(Alignment.CenterVertically)
                        )
                        Column(modifier = Modifier
                            .align(Alignment.CenterVertically)
                            .padding(16.dp)) {
                            Text(
                                modifier = Modifier,
                                text = "User Name", fontSize = 20.sp,
                                color = Color.White,
                                textAlign = TextAlign.Center
                            )
                            Spacer(modifier = Modifier.fillMaxHeight(0.2f))
                            Text(
                                modifier = Modifier,
                                text = "diodio10101@gmail.com", fontSize = 16.sp,
                                color = Color.White,
                                textAlign = TextAlign.Center
                            )
                        }
                        
                    }
                }

            }
            Column(modifier = Modifier.fillMaxSize()) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp)
                    .background(Color.White)) {
                    Image(
                        modifier = Modifier.padding(16.dp),
                        painter = painterResource(id = R.drawable.ic_basket), contentDescription = null
                    )
                    Text(
                        fontSize = 20.sp,
                        text = "Buyurtmalarim", modifier = Modifier.align(Alignment.CenterVertically)
                    )
                    Box(modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)){
                        Icon(
                            painter = painterResource(id = R.drawable.forward), contentDescription = null,
                            modifier = Modifier.align(Alignment.CenterEnd)
                        )

                    }
                }
                Button(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(32.dp),
                    onClick = {
                    
                },
                ) {
                  Text(text = "Chikish", color = Color.White)  
                }
                Text(text = "versiya: 1.6.2", modifier = Modifier.align(Alignment.CenterHorizontally))
            }

        }
    }
}