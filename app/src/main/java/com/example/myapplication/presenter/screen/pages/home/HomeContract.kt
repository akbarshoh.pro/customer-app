package com.example.myapplication.presenter.screen.pages.home

import org.orbitmvi.orbit.ContainerHost

sealed class HomeContract {
    interface Model : ContainerHost<UIState, SideEffect> {
        fun onEventDispatcher(intent: Intent)
    }

    sealed interface UIState {
        object InitState: UIState
    }

    sealed interface SideEffect {
        data class Toast(val message: String): SideEffect
    }

    sealed interface Intent {

    }

}