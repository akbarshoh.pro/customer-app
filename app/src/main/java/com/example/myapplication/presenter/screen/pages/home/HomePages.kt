package com.example.myapplication.presenter.screen.pages.home

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material3.SearchBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import com.example.myapplication.R
import com.example.myapplication.data.model.CategoryByProductData
import com.example.myapplication.data.model.ProductByMainData
import com.example.myapplication.ui.theme.MyApplicationTheme

object HomePages:Tab {
    override val options: TabOptions
        @Composable
        get() {
            val title = "Home"
            val icon = rememberVectorPainter(image = ImageVector.vectorResource(id = R.drawable.ic_home))

            return remember {
                TabOptions(
                    index = 0u,
                    title = title,
                    icon = icon
                )
            }
        }

    @Composable
    override fun Content() {
        MyApplicationTheme {
            HomeContent()
        }
    }
}

@Composable
fun HomeContent() {
    Surface(
        modifier = Modifier.fillMaxSize()
    ) {
       Column(modifier = Modifier.fillMaxSize()) {
           Column(modifier = Modifier.fillMaxWidth()) {

           }
       }
    }
}
val productData= arrayListOf(
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
    ProductByMainData("", arrayListOf("https://firebasestorage.googleapis.com/v0/b/market-app-b7.appspot.com/o/images%2Fimage0?alt=media&token=3d984b21-4ee0-45eb-9b6b-82920e9029b9"),"Kiyim","Birbolo","Saas","Odam bosa kere","100 som"),
)

val data= arrayListOf(
    CategoryByProductData("","Kiyim", productData),
    CategoryByProductData("","Kiyim", productData),
    CategoryByProductData("","Kiyim", productData),
    CategoryByProductData("","Kiyim", productData),
)

