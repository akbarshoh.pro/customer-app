package com.example.myapplication.data.model

data class BasketData(
    var productsId:ArrayList<String>,
    var countsArr:ArrayList<Int>,
)
