package com.example.myapplication.data.model

import android.os.Parcelable
import com.example.myapplication.data.model.ProductByMainData
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoryByProductData(
    val categoryId : String,
    val categoryName : String,
    val productsList : List<ProductByMainData>
) : Parcelable
