package com.example.myapplication.data.model

data class UserData(
    val id:String,
    val name:String,
    val gmail:String,
    val password:String,
    val type:String
)
