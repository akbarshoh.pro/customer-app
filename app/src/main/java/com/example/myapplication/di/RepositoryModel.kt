package com.example.uzummarketclient.di

import com.example.myapplication.domain.BasketRepository
import com.example.myapplication.domain.CategoryByPrRepository
import com.example.myapplication.domain.HomeRepository
import com.example.myapplication.domain.LoginRepository
import com.example.myapplication.domain.OrdersRepository
import com.example.myapplication.domain.impl.BasketRepositoryImpl
import com.example.myapplication.domain.impl.CategoryByPrRepositoryImpl
import com.example.myapplication.domain.impl.HomeRepositoryImpl
import com.example.myapplication.domain.impl.LoginRepositoryImpl
import com.example.myapplication.domain.impl.OrdersRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModel {
    @[Binds Singleton]
    fun bindLogin(impl: LoginRepositoryImpl): LoginRepository

    @[Binds Singleton]
    fun bindHomeScreen(impl : HomeRepositoryImpl) : HomeRepository

    @[Binds Singleton]
    fun bindCategoryByProduct(impl: CategoryByPrRepositoryImpl) : CategoryByPrRepository
    @[Binds Singleton]
    fun bindBasketScreen(impl : BasketRepositoryImpl) : BasketRepository

    @[Binds Singleton]
    fun bindOrderScreen(impl : OrdersRepositoryImpl) : OrdersRepository
}